<?php

namespace Drupal\commerce_trustpay\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Annotation\CommercePaymentMethodType;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the PayPal payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_trustpay",
 *   label = @Translation("Trustpay payment"),
 *   create_label = @Translation("New Trustpay payment"),
 * )
 */
class Trustpay extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return "Trustpay";
  }
}
